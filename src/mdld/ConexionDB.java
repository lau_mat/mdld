/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdld;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Carlos Laureano
 */
public class ConexionDB {
    private static String base = "";//Base de Datos
    private static String usuario = "";//usuario
    private static String pass = "";//contraseña
    private static String ruta = "";//host
    Connection conexion = null;

   ConexionDB() {
        config();
        try {
            //obtenemos el driver de para mysql
            Class.forName("com.mysql.jdbc.Driver");
            //obtenemos la conexión
            conexion = DriverManager.getConnection(ruta, usuario, pass);
            if (conexion != null) {
                System.out.println("Conexión a base de datos: " + base + ". listo");
            }
        } catch (SQLException e) {
            System.out.println(e);

        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
    }

    private void config() {
        Properties propiedades = new Properties();
        InputStream entrada = null;

        try {
            entrada = new FileInputStream("./src/ArchivosConfiguracion/ConfiguracionBD.properties");
            // cargamos el archivo de propiedades
            propiedades.load(entrada);
            // obtenemos las propiedades y las imprimimos
            base = propiedades.getProperty("BD");
            usuario = propiedades.getProperty("User");
            pass = propiedades.getProperty("Psw");
            ruta = propiedades.getProperty("Servidor") + base;
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (entrada != null) {
                try {
                    entrada.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Permite retornar la conexión
     */
    protected Connection getConnection() {
        return conexion;
    }

    /**
     * Permite desconectar conexión
     */
    protected void desconectar() {
        try {
            conexion.close();
        } catch (SQLException ex) {
            System.out.println("error: " + ex.getMessage());
        }
        System.out.println("La conexion a la  base de datos: " + base + " a terminado");
    }
}
